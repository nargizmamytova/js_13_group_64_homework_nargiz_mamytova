import { Component, OnInit } from '@angular/core';
import { Post } from '../shared/post.modal';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  contacts!: [];
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<[]>('https://plovo-bb6ec-default-rtdb.firebaseio.com/contacts.json')
    .subscribe(contacts => {
        this.contacts = contacts;
    })

  }

}
