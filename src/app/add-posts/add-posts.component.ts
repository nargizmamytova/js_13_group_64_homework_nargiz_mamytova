import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Post } from '../shared/post.modal';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-posts',
  templateUrl: './add-posts.component.html',
  styleUrls: ['./add-posts.component.css']
})
export class AddPostsComponent implements OnInit {
  title: string = '';
  description: string = '';
  post! : Post;
  postId: number|null = null
  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if(params['id']){
        const postId = this.route.snapshot.params['id'];
        this.postId = postId;
        this.http.get<Post>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/posts/${postId}.json`)
          .pipe()
          .subscribe(results => {
            this.post  = results;
            this.title = results.title;
            this.description = results.description;
          })
      }
    })
  }

  onSave() {
    const newDate = new Date();
    const date =  newDate.toDateString();
    const title = this.title;
    const description = this.description
    let body = {title, date, description };
    this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/posts.json', body)
      .subscribe();
    this.title = '';
    this.description = '';
  }

}
