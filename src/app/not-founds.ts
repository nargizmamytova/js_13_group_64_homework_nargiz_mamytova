import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>We can't found this page!!</h1>`
})
export class NotFounds{}

