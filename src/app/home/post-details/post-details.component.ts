import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, Routes } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../shared/post.modal';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;
  constructor(private route: ActivatedRoute, private http: HttpClient,  private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = this.route.snapshot.params['id'];
      this.http.get<Post>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/posts/${postId}.json`)
        .pipe()
        .subscribe(results => {
          this.post = results;
        })
    })
  }

  onDelete() {
    this.route.params.subscribe((params: Params) => {
      const postId = this.route.snapshot.params['id'];
      this.http.delete(`https://plovo-bb6ec-default-rtdb.firebaseio.com/posts/${postId}.json`).subscribe()
    })
    void this.router.navigate(['/']);
  }

  onEdit() {
    this.route.params.subscribe((params: Params) => {
      this.post.id = this.route.snapshot.params['id'];
    })
  }
}
