import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../shared/post.modal';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allPosts!: Post[];
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<{[id: string]: Post}>('https://plovo-bb6ec-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result => {
        if(result === null){
          return [];
        }
        return  Object.keys(result).map(id => {
          const postData = result[id];
          return new Post(
            id,
            postData.title,
            postData.date,
            postData.description
          )
        });
      }))
      .subscribe(posts => {
        this.allPosts = posts;
      })

  }

}
